package com.anhj.financialmanagement.repository;

import com.anhj.financialmanagement.entity.CarTrim;
import com.anhj.financialmanagement.enums.ExternalType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarTrimRepository extends JpaRepository<CarTrim, Long> {
    List<CarTrim> findAllByExternalTypeOrderByIdDesc(ExternalType externalType);
}
