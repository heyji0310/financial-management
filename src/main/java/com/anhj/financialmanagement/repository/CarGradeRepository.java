package com.anhj.financialmanagement.repository;

import com.anhj.financialmanagement.entity.CarGrade;
import com.anhj.financialmanagement.enums.FuelType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarGradeRepository extends JpaRepository<CarGrade, Long> {
    List<CarGrade> findAllByFuelTypeOrderByIdDesc(FuelType fuelType);
}
