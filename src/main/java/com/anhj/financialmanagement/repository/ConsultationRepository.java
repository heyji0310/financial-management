package com.anhj.financialmanagement.repository;

import com.anhj.financialmanagement.entity.Consultation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultationRepository extends JpaRepository<Consultation, Long> {
}
