package com.anhj.financialmanagement.repository;

import com.anhj.financialmanagement.entity.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {
}