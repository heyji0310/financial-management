package com.anhj.financialmanagement.repository;

import com.anhj.financialmanagement.entity.Manufacturer;
import com.anhj.financialmanagement.enums.ManufacturerType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {
    List<Manufacturer> findAllByManufacturerTypeOrderByIdDesc(ManufacturerType manufacturerType);
}
