package com.anhj.financialmanagement.service;

import com.anhj.financialmanagement.entity.CarGrade;
import com.anhj.financialmanagement.entity.CarModel;
import com.anhj.financialmanagement.enums.FuelType;
import com.anhj.financialmanagement.exception.CMissingDataException;
import com.anhj.financialmanagement.model.CarGradeDetail;
import com.anhj.financialmanagement.model.CarGradeItem;
import com.anhj.financialmanagement.model.CarGradeRequest;
import com.anhj.financialmanagement.model.ListResult;
import com.anhj.financialmanagement.repository.CarGradeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarGradeService {
    private final CarGradeRepository carGradeRepository;

    public CarGrade getCarGradeData(long id) {
        return carGradeRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setCarGrade(CarModel carModel, CarGradeRequest request) {
        CarGrade carGrade = new CarGrade.CarGradeBuilder(carModel, request).build();
        carGradeRepository.save(carGrade);
    }

    public CarGradeDetail getCarGrade(long id) {
        CarGrade carGrade = carGradeRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new CarGradeDetail.CarGradeDetailBuilder(carGrade).build();
    }

    public ListResult<CarGradeItem> getCarGrades() {
        List<CarGrade> carGrades = carGradeRepository.findAll();
        List<CarGradeItem> result = new LinkedList<>();

        carGrades.forEach(carGrade -> {
            CarGradeItem addItem = new CarGradeItem.CarGradeItemBuilder(carGrade).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public ListResult<CarGradeItem> getCarGrades(FuelType fuelType) {
        List<CarGrade> carGrades = carGradeRepository.findAllByFuelTypeOrderByIdDesc(fuelType);
        List<CarGradeItem> result = new LinkedList<>();

        carGrades.forEach(carGrade -> {
            CarGradeItem addItem = new CarGradeItem.CarGradeItemBuilder(carGrade).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
