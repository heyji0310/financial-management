package com.anhj.financialmanagement.service;

import com.anhj.financialmanagement.entity.CarTrim;
import com.anhj.financialmanagement.entity.Consultation;
import com.anhj.financialmanagement.exception.CMissingDataException;
import com.anhj.financialmanagement.model.ConsultationDetail;
import com.anhj.financialmanagement.model.ConsultationRequest;
import com.anhj.financialmanagement.model.ConsultationItem;
import com.anhj.financialmanagement.model.ListResult;
import com.anhj.financialmanagement.repository.ConsultationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConsultationService {
    private final ConsultationRepository consultationRepository;

    public void setConsultation(CarTrim carTrim, ConsultationRequest request) {
        Consultation consultationDetail = new Consultation.ConsultationBuilder(carTrim, request).build();
        consultationRepository.save(consultationDetail);
    }

    public ConsultationDetail getConsultation (long id) {
        Consultation consultation = consultationRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new ConsultationDetail.ConsultationDetailBuilder(consultation).build();
    }

    public ListResult<ConsultationItem> getConsultations() {
        List<Consultation> consultations = consultationRepository.findAll();
        List<ConsultationItem> result = new LinkedList<>();

        consultations.forEach(consultation -> {
            ConsultationItem addItem = new ConsultationItem.ConsultationItemBuilder(consultation).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
