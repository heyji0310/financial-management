package com.anhj.financialmanagement.service;

import com.anhj.financialmanagement.entity.Manufacturer;
import com.anhj.financialmanagement.enums.ManufacturerType;
import com.anhj.financialmanagement.exception.CMissingDataException;
import com.anhj.financialmanagement.model.*;
import com.anhj.financialmanagement.repository.ManufacturerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ManufacturerService {
    private final ManufacturerRepository manufacturerRepository;

    public Manufacturer getManufacturerData(long id) {
        return manufacturerRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setManufacturer(ManufacturerRequest request) {
        Manufacturer manufacturer = new Manufacturer.ManufacturerBuilder(request).build();
        manufacturerRepository.save(manufacturer);
    }

    public ManufacturerDetail getManufacturer(long id) {
        Manufacturer manufacturer = manufacturerRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new ManufacturerDetail.ManufacturerDetailBuilder(manufacturer).build();
    }

    public ListResult<ManufacturerItem> getManufacturers() {
        List<Manufacturer> manufacturers = manufacturerRepository.findAll();
        List<ManufacturerItem> result =new LinkedList<>();
        manufacturers.forEach(manufacturer -> {
            ManufacturerItem addItem = new ManufacturerItem.ManufacturerItemBuilder(manufacturer).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public ListResult<ManufacturerItem> getManufacturers(ManufacturerType manufacturerType) {
        List<Manufacturer> manufacturers = manufacturerRepository.findAllByManufacturerTypeOrderByIdDesc(manufacturerType);
        List<ManufacturerItem> result =new LinkedList<>();
        manufacturers.forEach(manufacturer -> {
            ManufacturerItem addItem = new ManufacturerItem.ManufacturerItemBuilder(manufacturer).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public void putManufacturerLogoImage(long id, ManufacturerLogoImageUpdateRequest updateRequest) {
        Manufacturer manufacturer = manufacturerRepository.findById(id).orElseThrow(CMissingDataException::new);
        manufacturer.putLogoImage(updateRequest);
        manufacturerRepository.save(manufacturer);
    }
}
