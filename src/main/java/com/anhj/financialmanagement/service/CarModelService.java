package com.anhj.financialmanagement.service;

import com.anhj.financialmanagement.entity.CarModel;
import com.anhj.financialmanagement.entity.Manufacturer;
import com.anhj.financialmanagement.exception.CMissingDataException;
import com.anhj.financialmanagement.model.*;
import com.anhj.financialmanagement.repository.CarModelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarModelService {
    private final CarModelRepository carModelRepository;

    public CarModel getCarModelData(long id) {
        return carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setCarModel(Manufacturer manufacturer, CarModelRequest request) {
        CarModel carModel = new CarModel.CarModelBuilder(manufacturer, request).build();
        carModelRepository.save(carModel);
    }

    public CarModelDetail getCarModel(long id) {
        CarModel carModel = carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new CarModelDetail.CarModelDetailBuilder(carModel).build();
    }

    public ListResult<CarModelItem> getCarModels() {
        List<CarModel> carModels = carModelRepository.findAll();
        List<CarModelItem> result = new LinkedList<>();

        carModels.forEach(carModel -> {
            CarModelItem addItem = new CarModelItem.CarModelItemBuilder(carModel).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public void putCarModelName(long id, CarModelNameUpdateRequest updateRequest) {
        CarModel carModel = carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
        carModel.putCarModelNameData(updateRequest);
        carModelRepository.save(carModel);
    }
}
