package com.anhj.financialmanagement.service;

import com.anhj.financialmanagement.entity.CarGrade;
import com.anhj.financialmanagement.entity.CarTrim;
import com.anhj.financialmanagement.enums.ExternalType;
import com.anhj.financialmanagement.exception.CMissingDataException;
import com.anhj.financialmanagement.model.CarTrimDetail;
import com.anhj.financialmanagement.model.CarTrimItem;
import com.anhj.financialmanagement.model.CarTrimRequest;
import com.anhj.financialmanagement.model.ListResult;
import com.anhj.financialmanagement.repository.CarTrimRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarTrimService {
    private final CarTrimRepository carTrimRepository;

    public CarTrim getCarTrimData(long id) {
        return carTrimRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setCarTrim(CarGrade carGrade, CarTrimRequest request) {
        CarTrim carTrim = new CarTrim.CarTrimBuilder(carGrade, request).build();
        carTrimRepository.save(carTrim);
    }

    public CarTrimDetail getCarTrim(long id) {
        CarTrim carTrim = carTrimRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new CarTrimDetail.CarTrimDetailBuilder(carTrim).build();
    }

    public ListResult<CarTrimItem> getCarTrims() {
        List<CarTrim> carTrims = carTrimRepository.findAll();
        List<CarTrimItem> result = new LinkedList<>();

        carTrims.forEach(carTrim -> {
            CarTrimItem addItem = new CarTrimItem.CarTrimItemBuilder(carTrim).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public ListResult<CarTrimItem> getCarTrims(ExternalType externalType) {
        List<CarTrim> carTrims = carTrimRepository.findAllByExternalTypeOrderByIdDesc(externalType);
        List<CarTrimItem> result = new LinkedList<>();

        carTrims.forEach(carTrim -> {
            CarTrimItem addItem = new CarTrimItem.CarTrimItemBuilder(carTrim).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
