package com.anhj.financialmanagement.entity;

import com.anhj.financialmanagement.interfaces.CommonModelBuilder;
import com.anhj.financialmanagement.model.CarModelNameUpdateRequest;
import com.anhj.financialmanagement.model.CarModelRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manufacturerId", nullable = false)
    private Manufacturer manufacturer;

    @Column(nullable = false, length = 30)
    private String modelName;

    @Column(nullable = false)
    private LocalDate createYear;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putCarModelNameData(CarModelNameUpdateRequest updateRequest) {
        this.modelName = updateRequest.getCarModelName();
        this.dateUpdate = LocalDateTime.now();
    }

    public CarModel(CarModelBuilder builder) {
        this.manufacturer = builder.manufacturer;
        this.modelName = builder.modelName;
        this.createYear = builder.createYear;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarModelBuilder implements CommonModelBuilder<CarModel> {
        private final Manufacturer manufacturer;
        private final String modelName;
        private final LocalDate createYear;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarModelBuilder(Manufacturer manufacturer, CarModelRequest request) {
            this.manufacturer = manufacturer;
            this.modelName = request.getModelName();
            this.createYear = request.getCreateYear();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarModel build() {
            return new CarModel(this);
        }
    }
}
