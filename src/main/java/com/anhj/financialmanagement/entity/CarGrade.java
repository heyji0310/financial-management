package com.anhj.financialmanagement.entity;

import com.anhj.financialmanagement.enums.FuelType;
import com.anhj.financialmanagement.interfaces.CommonModelBuilder;
import com.anhj.financialmanagement.model.CarGradeRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarGrade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carModelId", nullable = false)
    private CarModel carModel;

    @Column(nullable = false, length = 30)
    private String gradeName;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private FuelType fuelType;

    @Column(nullable = false)
    private Integer displacement;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private CarGrade(CarGradeBuilder builder) {
        this.carModel = builder.carModel;
        this.gradeName = builder.gradeName;
        this.fuelType = builder.fuelType;
        this.displacement = builder.displacement;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarGradeBuilder implements CommonModelBuilder<CarGrade> {
        private final CarModel carModel;
        private final String gradeName;
        private final FuelType fuelType;
        private final Integer displacement;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarGradeBuilder(CarModel carModel, CarGradeRequest request) {
            this.carModel = carModel;
            this.gradeName = request.getGradeName();
            this.fuelType = request.getFuelType();
            this.displacement = request.getDisplacement();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarGrade build() {
            return new CarGrade(this);
        }
    }
}
