package com.anhj.financialmanagement.entity;

import com.anhj.financialmanagement.interfaces.CommonModelBuilder;
import com.anhj.financialmanagement.model.ConsultationRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Consultation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carTrimId", nullable = false)
    private CarTrim carTrim;

    @Column(nullable = false, length = 20)
    private String customerName;

    @Column(nullable = false, length = 15)
    private String customerPhone;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    private Consultation(ConsultationBuilder builder) {
        this.carTrim = builder.carTrim;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.dateCreate = builder.dateCreate;
    }

    public static class ConsultationBuilder implements CommonModelBuilder<Consultation> {
        private final CarTrim carTrim;
        private final String customerName;
        private final String customerPhone;
        private final LocalDateTime dateCreate;

        public ConsultationBuilder(CarTrim carTrim, ConsultationRequest request) {
            this.carTrim = carTrim;
            this.customerName = request.getCustomerName();
            this.customerPhone = request.getCustomerPhone();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public Consultation build() {
            return new Consultation(this);
        }
    }
}
