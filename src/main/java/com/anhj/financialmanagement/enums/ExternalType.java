package com.anhj.financialmanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExternalType {
    PASSENGER_CAR("승용차")
    , VAN("승합차")
    , SUV("suv")
    , TRUCK("트럭")
    , SUB_COMPACT("경차")
    , MEDIUM_SEDAN("중형세단")
    ;

    private final String externalTypeName;
}
