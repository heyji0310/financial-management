package com.anhj.financialmanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FuelType {
    GASOLINE("가솔린")
    , DIESEL("디젤")
    , LPG("엘피쥐")
    , HYBRID("하이브리드")
    , ELECTRIC("전기차")
    ;

    private final String fuelTypeName;
}
