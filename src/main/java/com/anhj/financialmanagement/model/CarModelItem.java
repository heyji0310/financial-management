package com.anhj.financialmanagement.model;

import com.anhj.financialmanagement.entity.CarModel;
import com.anhj.financialmanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModelItem {
    @ApiModelProperty(notes = "차량모델 시퀀스")
    private Long carModelId;

    @ApiModelProperty(notes = "차량 모델 풀네임([제조사명] + 모델명, yyyy연식")
    private String carModelFullName;

    public CarModelItem(CarModelItemBuilder builder) {
        this.carModelId = builder.carModerId;
        this.carModelFullName = builder.carModerFullName;
    }

    public static class CarModelItemBuilder implements CommonModelBuilder<CarModelItem> {
        private final Long carModerId;
        private final String carModerFullName;

        public CarModelItemBuilder(CarModel carModel) {
            this.carModerId = carModel.getId();
            this.carModerFullName = "[" + carModel.getManufacturer().getManufacturerType().getManufacturerTypeName() + "] " + carModel.getModelName() + ", " + carModel.getCreateYear().getYear() + "년식";
        }

        @Override
        public CarModelItem build() {
            return new CarModelItem(this);
        }
    }
}
