package com.anhj.financialmanagement.model;

import com.anhj.financialmanagement.entity.CarModel;
import com.anhj.financialmanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModelDetail {
    @ApiModelProperty(notes = "차량모델 시퀀스")
    private Long carModelId;

    @ApiModelProperty(notes = "제조사명")
    private String manufacturer;

    @ApiModelProperty(notes = "모델명")
    private String modelName;

    @ApiModelProperty(notes = "모델 연식")
    private String createYear;

    private CarModelDetail(CarModelDetailBuilder builder) {
        this.carModelId = builder.carModelId;
        this.manufacturer = builder.manufacturer;
        this.modelName = builder.modelName;
        this.createYear = builder.createYear;
    }

    public static class CarModelDetailBuilder implements CommonModelBuilder<CarModelDetail> {
        private final Long carModelId;
        private final String manufacturer;
        private final String modelName;
        private final String  createYear;

        public CarModelDetailBuilder(CarModel carModel) {
            this.carModelId = carModel.getId();
            this.manufacturer = carModel.getManufacturer().getManufacturerType().getManufacturerTypeName();
            this.modelName = carModel.getModelName();
            this.createYear = carModel.getCreateYear().getYear() + "연식";
        }

        @Override
        public CarModelDetail build() {
            return new CarModelDetail(this);
        }
    }
}
