package com.anhj.financialmanagement.model;

import com.anhj.financialmanagement.entity.CarTrim;
import com.anhj.financialmanagement.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarTrimItem {
    private Long carGradeId;
    private String trimName;
    private String price;
    private String externalType;
    private String fuelFullInfo;
    private String capacity;

    private CarTrimItem(CarTrimItemBuilder builder) {
        this.carGradeId = builder.carGradeId;
        this.trimName = builder.trimName;
        this.price = builder.price;
        this.externalType = builder.externalType;
        this.fuelFullInfo = builder.fuelFullInfo;
        this.capacity = builder.capacity;
    }

    public static class CarTrimItemBuilder implements CommonModelBuilder<CarTrimItem> {
        private final Long carGradeId;
        private final String trimName;
        private final String price;
        private final String externalType;
        private final String fuelFullInfo;
        private final String capacity;

        public CarTrimItemBuilder(CarTrim carTrim) {
            this.carGradeId = carTrim.getId();
            this.trimName = carTrim.getTrimName();
            this.price = carTrim.getPrice() + "만원";
            this.externalType = carTrim.getExternalType().getExternalTypeName();
            this.fuelFullInfo = "복합: " + carTrim.getCombinedFuel() + "km/l, 도심: " + carTrim.getCityFuel() + "km/l, 고속도로: " + carTrim.getHighwayFuel() + "km/l, " + carTrim.getFuelGrade() + "등급";
            this.capacity = carTrim.getCapacity() + "인승";
        }

        @Override
        public CarTrimItem build() {
            return new CarTrimItem(this);
        }
    }
}
