package com.anhj.financialmanagement.model;

import com.anhj.financialmanagement.entity.Consultation;
import com.anhj.financialmanagement.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConsultationItem {
    private Long carTrimId;
    private String trimInfo;
    private String customerInfo;

    private ConsultationItem(ConsultationItemBuilder builder) {
        this.carTrimId = builder.carTrimId;
        this.trimInfo = builder.trimInfo;
        this.customerInfo = builder.customerInfo;
    }

    public static class ConsultationItemBuilder implements CommonModelBuilder<ConsultationItem> {
        private final Long carTrimId;
        private final String trimInfo;
        private final String customerInfo;

        public ConsultationItemBuilder(Consultation consultation) {
            this.carTrimId = consultation.getId();
            this.trimInfo = "[" + consultation.getCarTrim().getExternalType().getExternalTypeName() + "] " + consultation.getCarTrim().getTrimName() + consultation.getCarTrim().getPrice() + "만원";
            this.customerInfo = consultation.getCustomerName() + ", " + consultation.getCustomerPhone();
        }

        @Override
        public ConsultationItem build() {
            return new ConsultationItem(this);
        }
    }
}
