package com.anhj.financialmanagement.model;

import com.anhj.financialmanagement.enums.FuelType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarGradeRequest {
    @ApiModelProperty(notes = "차량 모델 id", required = true)
    @NotNull
    private Long carModelId;

    @ApiModelProperty(notes = "등급명", required = true)
    @NotNull
    @Length(max = 30)
    private String gradeName;

    @ApiModelProperty(notes = "연료 타입", required = true)
    @NotNull
    private FuelType fuelType;

    @ApiModelProperty(notes = "배기량", required = true)
    @NotNull
    private Integer displacement;
}
