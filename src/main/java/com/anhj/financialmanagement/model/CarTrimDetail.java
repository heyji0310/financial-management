package com.anhj.financialmanagement.model;

import com.anhj.financialmanagement.entity.CarTrim;
import com.anhj.financialmanagement.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarTrimDetail {
    private Long carGradeId;
    private String trimName;
    private String price;
    private String externalType;
    private String combinedFuel;
    private String cityFuel;
    private String highwayFuel;
    private String fuelGrade;
    private String capacity;

    private CarTrimDetail(CarTrimDetailBuilder builder) {
        this.carGradeId = builder.carGradeId;
        this.trimName = builder.trimName;
        this.price = builder.price;
        this.externalType = builder.externalType;
        this.combinedFuel = builder.combinedFuel;
        this.cityFuel = builder.cityFuel;
        this.highwayFuel = builder.highwayFuel;
        this.fuelGrade = builder.fuelGrade;
        this.capacity = builder.capacity;
    }

    public static class CarTrimDetailBuilder implements CommonModelBuilder<CarTrimDetail> {
        private final Long carGradeId;
        private final String trimName;
        private final String price;
        private final String externalType;
        private final String combinedFuel;
        private final String cityFuel;
        private final String highwayFuel;
        private final String fuelGrade;
        private final String capacity;

        public CarTrimDetailBuilder(CarTrim carTrim) {
            this.carGradeId = carTrim.getId();
            this.trimName = carTrim.getTrimName();
            this.price = carTrim.getPrice() + "만원";
            this.externalType = carTrim.getExternalType().getExternalTypeName();
            this.combinedFuel = carTrim.getCombinedFuel() + "km/l";
            this.cityFuel = carTrim.getCityFuel() + "km/l";
            this.highwayFuel = carTrim.getHighwayFuel() + "km/l";
            this.fuelGrade = carTrim.getFuelGrade() + "등급";
            this.capacity = carTrim.getCapacity() + "인승";
        }

        @Override
        public CarTrimDetail build() {
            return new CarTrimDetail(this);
        }
    }
}
