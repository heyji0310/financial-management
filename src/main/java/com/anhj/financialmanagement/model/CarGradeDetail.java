package com.anhj.financialmanagement.model;

import com.anhj.financialmanagement.entity.CarGrade;
import com.anhj.financialmanagement.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarGradeDetail {
    private Long carGradeId;

    private String carModelName;

    private String carGradeName;

    private String fuelType;

    private String displacement;

    private CarGradeDetail(CarGradeDetailBuilder builder) {
        this.carGradeId = builder.carGradeId;
        this.carModelName = builder.carModelName;
        this.carGradeName = builder.carGradeName;
        this.fuelType = builder.fuelType;
        this.displacement = builder.displacement;
    }

    public static class CarGradeDetailBuilder implements CommonModelBuilder<CarGradeDetail> {
        private final Long carGradeId;
        private final String carModelName;
        private final String carGradeName;
        private final String fuelType;
        private final String displacement;

        public CarGradeDetailBuilder(CarGrade carGrade) {
            this.carGradeId = carGrade.getId();
            this.carModelName = "[" + carGrade.getCarModel().getManufacturer().getManufacturerType().getManufacturerTypeName() + "] " + carGrade.getCarModel().getModelName() + ", " + carGrade.getCarModel().getCreateYear().getYear() + "연식";
            this.carGradeName = carGrade.getGradeName();
            this.fuelType = carGrade.getFuelType().getFuelTypeName();
            this.displacement = carGrade.getDisplacement() + "cc";
        }

        @Override
        public CarGradeDetail build() {
            return new CarGradeDetail(this);
        }
    }
}
