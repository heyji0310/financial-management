package com.anhj.financialmanagement.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class CarModelRequest {
    @ApiModelProperty(notes = "제조사 id", required = true)
    @NotNull
    private Long manufacturerId;

    @ApiModelProperty(notes = "모델명", required = true)
    @NotNull
    @Length(max = 30)
    private String modelName;

    @ApiModelProperty(notes = "연식", required = true)
    @NotNull
    private LocalDate createYear;
}