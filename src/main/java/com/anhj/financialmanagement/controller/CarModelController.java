package com.anhj.financialmanagement.controller;

import com.anhj.financialmanagement.entity.Manufacturer;
import com.anhj.financialmanagement.model.*;
import com.anhj.financialmanagement.service.CarModelService;
import com.anhj.financialmanagement.service.ManufacturerService;
import com.anhj.financialmanagement.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "차량 모델 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car-model")
public class CarModelController {
    private final CarModelService carModelService;
    private final ManufacturerService manufacturerService;

    @ApiOperation(value = "차량 모델 등록")
    @PostMapping("/new")
    public CommonResult setCarModel(@RequestBody @Valid CarModelRequest modelRequest) {
        Manufacturer manufacturer = manufacturerService.getManufacturerData(modelRequest.getManufacturerId());

        carModelService.setCarModel(manufacturer, modelRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 모델 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "차량모델 시퀀스", required = true)
    )
    @GetMapping("/{id}")
    public SingleResult<CarModelDetail> getCarModel(@PathVariable long id) {
        return ResponseService.getSingleResult(carModelService.getCarModel(id));
    }

    @ApiOperation(value = "차량 모델 전체조회")
    @GetMapping("/all")
    public ListResult<CarModelItem> getCarModels() {
        return ResponseService.getListResult(carModelService.getCarModels(), true);
    }

    @ApiOperation(value = "차량 모델 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "차량모델 시퀀스", required = true)
    )
    @PutMapping("/{id}")
    public CommonResult putCarModelName(@PathVariable long id, @RequestBody @Valid CarModelNameUpdateRequest updateRequest) {
        carModelService.putCarModelName(id, updateRequest);
        return ResponseService.getSuccessResult();
    }
}
