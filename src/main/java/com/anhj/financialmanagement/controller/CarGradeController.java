package com.anhj.financialmanagement.controller;

import com.anhj.financialmanagement.entity.CarModel;
import com.anhj.financialmanagement.enums.FuelType;
import com.anhj.financialmanagement.model.*;
import com.anhj.financialmanagement.service.CarGradeService;
import com.anhj.financialmanagement.service.CarModelService;
import com.anhj.financialmanagement.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "차량 등급 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car-grade")
public class CarGradeController {
    private final CarModelService carModelService;
    private final CarGradeService carGradeService;

    @ApiOperation(value = "차량 등급 등록")
    @PostMapping("/new")
    public CommonResult setCarGrade(@RequestBody @Valid CarGradeRequest gradeRequest) {
        CarModel carModel = carModelService.getCarModelData(gradeRequest.getCarModelId());

        carGradeService.setCarGrade(carModel, gradeRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 등급 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "차량등급 시퀀스", required = true)
    )
    @GetMapping("/{id}")
    public SingleResult<CarGradeDetail> getCarGrade(@PathVariable long id) {
        return ResponseService.getSingleResult(carGradeService.getCarGrade(id));
    }

    @ApiOperation(value = "연료타입별 조회")
    @GetMapping("/search")
    public ListResult<CarGradeItem> getCarGrades(@RequestParam(value = "fuelType", required = false)FuelType fuelType) {
        if (fuelType == null) {
            return ResponseService.getListResult(carGradeService.getCarGrades(), true);
        } else {
            return ResponseService.getListResult(carGradeService.getCarGrades(fuelType), true);
        }

    }
}
