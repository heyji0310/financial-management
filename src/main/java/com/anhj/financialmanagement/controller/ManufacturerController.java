package com.anhj.financialmanagement.controller;

import com.anhj.financialmanagement.entity.Manufacturer;
import com.anhj.financialmanagement.enums.ManufacturerType;
import com.anhj.financialmanagement.model.*;
import com.anhj.financialmanagement.service.ManufacturerService;
import com.anhj.financialmanagement.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "차량 제조사 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/manufacturer")
public class ManufacturerController {
    private final ManufacturerService manufacturerService;

    @ApiOperation(value = "제조사 정보 등록")
    @PostMapping("/new")
    public CommonResult setManufacturer(@RequestBody @Valid ManufacturerRequest manufacturerRequest) {
        manufacturerService.setManufacturer(manufacturerRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "제조사 정보 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "제조사 시퀀스", required = true)
    )
    @GetMapping("/{id}")
    public SingleResult<ManufacturerDetail> getManufacturer(@PathVariable long id) {
        return ResponseService.getSingleResult(manufacturerService.getManufacturer(id));
    }

    @ApiOperation(value = "제조사별 정보 조회")
    @GetMapping("/search")
    public ListResult<ManufacturerItem> getManufacturers(@RequestParam(value = "manufacturerType", required = false) ManufacturerType manufacturerType) {
        if (manufacturerType == null) {
            return ResponseService.getListResult(manufacturerService.getManufacturers(), true);
        } else {
            return ResponseService.getListResult(manufacturerService.getManufacturers(manufacturerType), true);
        }
    }

    @ApiOperation(value = "제조사 로고 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "제조사 시퀀스", required = true)
    )
    @PutMapping("/{id}")
    public CommonResult putManufacturerLogoImage(@PathVariable long id, @RequestBody @Valid ManufacturerLogoImageUpdateRequest updateRequest) {
        manufacturerService.putManufacturerLogoImage(id, updateRequest);
        return ResponseService.getSuccessResult();
    }
}
