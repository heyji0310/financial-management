package com.anhj.financialmanagement.controller;

import com.anhj.financialmanagement.entity.CarGrade;
import com.anhj.financialmanagement.enums.ExternalType;
import com.anhj.financialmanagement.model.*;
import com.anhj.financialmanagement.service.CarGradeService;
import com.anhj.financialmanagement.service.CarTrimService;
import com.anhj.financialmanagement.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "차량 트림 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car-trim")
public class CarTrimController {
    private final CarGradeService carGradeService;
    private final CarTrimService carTrimService;

    @ApiOperation(value = "차량 트림 등록")
    @PostMapping("/new")
    public CommonResult setCarTrim(@RequestBody @Valid CarTrimRequest trimRequest) {
        CarGrade carGrade = carGradeService.getCarGradeData(trimRequest.getCarGradeId());
        carTrimService.setCarTrim(carGrade, trimRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 트림 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "차량트림 시퀀스", required = true)
    )
    @GetMapping("/{id}")
    public SingleResult<CarTrimDetail> getCarTrim(@PathVariable long id) {
        return ResponseService.getSingleResult(carTrimService.getCarTrim(id));
    }

    @ApiOperation(value = "차량 외장별 조회")
    @GetMapping("/search")
    public ListResult<CarTrimItem> getCarTrims(@RequestParam(value = "externalType", required = false) ExternalType externalType) {
        if (externalType == null) {
            return ResponseService.getListResult(carTrimService.getCarTrims(), true);
        } else {
            return ResponseService.getListResult(carTrimService.getCarTrims(externalType), true);
        }
    }
}
